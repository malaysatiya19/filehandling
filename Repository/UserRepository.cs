﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepository : IUserRepository, IFile
    {
        List<User> users;
        public UserRepository()
        {
            users = new List<User>();
        }

        public bool IsUserNameExists(string userName, string fileName)
        {
            bool isUserAvailable = false;
            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    string rowLine;
                    while ((rowLine = sr.ReadLine()) != null)
                    {
                        string[] rowSplittedValues = rowLine.Split(',');
                        foreach (string userProperty in rowSplittedValues)
                        {
                            if (userProperty == userName)
                            {
                                isUserAvailable = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("File doesn't exist.");
            }
            return isUserAvailable;
        }

        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while ((rowLine=sr.ReadLine())!=null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public void RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            bool isUserNameExists = IsUserNameExists(user.Name, fileName);
            if (isUserNameExists)
            {
                throw new InvalidUserName("User already exists");
            }
            else
            {
                WriteContentToFile(user, fileName);
                Console.WriteLine("Registered Sucessfully");
            }
        }

        public void WriteContentToFile(User user, string fileName)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(fileName, true))
                {
                    sw.WriteLine($"{user}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("File doesn't exist.");
            }
        }
    }
}
