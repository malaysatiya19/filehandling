﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Exceptions
{
    internal class InvalidUserName:Exception
    {
        public InvalidUserName()
        {

        }
        public InvalidUserName(string message):base(message)
        {

        }
    }
}
