﻿using FileHandling.Exceptions;
using FileHandling.Model;
using FileHandling.Repository;

UserRepository userRepository = new UserRepository();
IUserRepository iUserRepository = (IUserRepository)userRepository;
Console.WriteLine("Welcome to User Registration Page");
User user = new User();
Console.WriteLine("Enter Id:");
user.Id = int.Parse(Console.ReadLine());
Console.WriteLine("Enter Name:");
user.Name = (Console.ReadLine());
Console.WriteLine("Enter City:");
user.City = (Console.ReadLine());
try
{
    iUserRepository.RegisterUser(user);
}
catch (InvalidUserName inuEx)
{
    Console.WriteLine(inuEx.Message);
}
//if (isUserRegistered)
//{
//    Console.WriteLine("Registration Successful.");
//}
//else
//{
//    Console.WriteLine("User Already Exists!");
//}

Console.WriteLine("Reading Contents from File.");
List<string> userContent = userRepository.ReadContentsFromFile("userDatabase.txt");
foreach (string userInfo in userContent)
{
    Console.WriteLine(userInfo);
}